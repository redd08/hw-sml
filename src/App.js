import React from 'react';
import PaymentForm from './components/PaymentForm/PaymentForm';
import './App.css';

function App() {
  return (
    <div className="App">
      <PaymentForm />
    </div>
  );
}

export default App;
