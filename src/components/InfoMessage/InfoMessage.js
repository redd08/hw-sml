import React from 'react';
import styles from './InfoMessage.module.scss'
import {ReactComponent as InfoIcon} from '../../icons/info.svg';
import {ReactComponent as CrossIcon} from '../../icons/cross.svg';
import cn from 'classnames';
const InfoMessage = (props) => {
    return (
      <>
      <label className={styles.infoIcon} onClick={props.checkInfo}>
        {props.show ? <CrossIcon/> : <InfoIcon />}
      </label>
      <div className={styles.wrapper}>
      <div className={cn(styles.popupMessage, {[styles.hidden]:!props.show})}>
          <div className={styles.arrow}></div>
          <div className={styles.content}>{props.children}</div>
        </div>
        </div>
      </>
    );
}

export default InfoMessage;