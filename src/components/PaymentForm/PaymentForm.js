import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import * as cn from 'classnames';
import styles from './PaymentForm.module.scss';
import InfoMessage from '../InfoMessage/InfoMessage';

class PaymentForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type: 'salary',
      withoutTax: true,
      salary: null,
      showInfo: false,
      taxRate: 0.13,
      currency: '₽',
    };
  }
  checkInfo = () => this.setState({ showInfo: !this.state.showInfo });
  checkRadio = e => this.setState({ type: e.target.value });
  changeTax = () => this.setState({ withoutTax: !this.state.withoutTax });
  changeInput = e => this.setState({ salary: e.target.value });
  render() {
    const { type, withoutTax, salary, currency, taxRate } = this.state;
    return (
      <div className={styles.paymentForm}>
        <form>
          <div className={styles.title}>
            <label>Сумма</label>
          </div>
          <div className={styles.radioGroup}>
            <div>
              <label>
                <Field
                  name="payment"
                  component="input"
                  type="radio"
                  value="salary"
                  onChange={this.checkRadio}
                  checked={type === 'salary'}
                />{' '}
                Оклад за месяц
              </label>
            </div>
            <div>
              <label>
                <Field
                  name="payment"
                  component="input"
                  type="radio"
                  value="min_wage"
                  onChange={this.checkRadio}
                  checked={type === 'min_wage'}
                />{' '}
                МРОТ
              </label>
              <InfoMessage
                text={this.state.infoMessage}
                show={this.state.showInfo}
                checkInfo={this.checkInfo}
              >
                МРОТ — минимальный размер оплаты труда. Разный для разных
                регионов.
              </InfoMessage>
            </div>
            <div>
              <label>
                <Field
                  name="payment"
                  component="input"
                  type="radio"
                  value="ppd"
                  onChange={this.checkRadio}
                  checked={type === 'ppd'}
                />{' '}
                Оплата за день
              </label>
            </div>
            <div>
              <label>
                <Field
                  name="payment"
                  component="input"
                  type="radio"
                  value="pph"
                  onChange={this.checkRadio}
                  checked={type === 'pph'}
                />{' '}
                Оплата за час
              </label>
            </div>
          </div>
          <div className={styles.checkBox} onClick={this.changeTax}>
            <label className={cn('lb', { active: withoutTax })}>
              Указать с НДФЛ
            </label>
            <Field
              name="tax"
              component="input"
              type="checkbox"
              className={styles.custom}
              checked={withoutTax}
            />{' '}
            <label className={cn('lb', { active: !withoutTax })}>
              Без НДФЛ
            </label>
          </div>
          <div className={styles.currency}>
            <Field
              name="salary"
              component="input"
              type="number"
              onChange={this.changeInput}
              value={salary}
              className={styles.currencyInput}
              max="10000000"
              min="0"
            />
            <label>₽</label>
          </div>
          {type === 'salary' && salary && (
            <div className={styles.salaryInfo}>
              <p>
                <b>
                  {withoutTax
                    ? Number(salary).toFixed()
                    : Number(salary * (1 - 0.13)).toFixed()}{' '}
                  {currency}
                </b>{' '}
                Cотрудник получит на руки
              </p>
              <p>
                <b>
                  {withoutTax
                    ? Number(salary / (1 - taxRate) - salary).toFixed()
                    : Number(salary * taxRate).toFixed()}{' '}
                  {currency}
                </b>{' '}
                НДФЛ, {taxRate * 100}% от оклада
              </p>
              {withoutTax}
              <p>
                <b>
                  {withoutTax
                    ? Number(salary / Number(1 - taxRate)).toFixed()
                    : Number(salary).toFixed()}{' '}
                  {currency}
                </b>{' '}
                За сотрудника
              </p>
            </div>
          )}
        </form>
      </div>
    );
  }
}

export default reduxForm({
  form: 'paymentForm', // a unique identifier for this form
})(PaymentForm);
